// server.js
const net = require('net');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const port = 8124;
var seed = 0.

const server = net.createServer((client) => {
    console.log('Client ' + Date.now() + '_' + ++seed + ' connected');

    client.setEncoding('utf8');

    client.on('data', (data) => {
        if (data == 'QA') {
            rl.question(data, (answer) => {
                if (answer == 'ACK') {
                    client.write('\r\nHello!\r\nRegards,\r\nServer\r\n');
                } else {
                    client.write('close');
                }
                rl.close();
            });
        }
    });
    client.on('end', () => console.log('Client disconnected'));
});

server.listen(port, () => {
    console.log(`Server listening on localhost:${port}`);
});